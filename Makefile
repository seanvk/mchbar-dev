CC=gcc
RM=rm -f

SRCS=mchbar_dev.c
OBJS=mchbar_dev.o
BINS=mchbar-dev
#CFLAGS += -O2 -Wall -W -I/usr/include/libdrm
CFLAGS += -g -Wall -W -I/usr/include/
LDLIBS += -lpciaccess


all: mchbar-dev

mchbar-dev: mchbar_dev.o
	$(CC) $(CFLAGS) -o mchbar-dev mchbar_dev.o $(LDLIBS)

.depend: $(SRCS)
	$(RM) ./.depend
	$(CC) $(CFLAGS) -MM $^>>./.depend;

clean:
	$(RM) $(OBJS)

distclean: clean
	$(RM) $(BINS) *~ .depend

include .depend
