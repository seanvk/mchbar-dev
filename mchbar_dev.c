/*
   cc -g -Wall -W -I/usr/include/     mchbar_dev.c  -lpciaccess  -o mchbar_dev
   */

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

#include "intel_chipset.h"
#include <pciaccess.h>

#define MCHBAR_I915 0x44
#define MCHBAR_I965 0x48
#define MCHBAR_SIZE (4 * 4096)

#define DEVEN_REG 0x54
#define DEVEN_MCHBAR_EN (1 << 28)

struct pci_device *
get_pci_device (void)
{
  struct pci_device *pci_dev;
  int error;

  /* Grab the graphics card */
  pci_dev = pci_device_find_by_slot (0, 0, 2, 0);
  if (pci_dev == NULL)
    {
      fprintf (stderr, "Couldn't find graphics card\n");
      exit (1);
    }

  error = pci_device_probe (pci_dev);
  if (error != 0)
    {
      fprintf (stderr, "Couldn't probe graphics card\n");
      exit (1);
    }

  if (pci_dev->vendor_id != 0x8086)
    {
      fprintf (stderr, "Graphics card is non-intel\n");
      exit (1);
    }
  printf ("PCI Device ID: 0x%x\n", pci_dev->device_id);
  return pci_dev;
}

int
main (void)
{
  struct pci_device *pci_dev;
  int error;
  uint32_t temp;
  pciaddr_t mchbar_reg;
  int enabled = 0;

  error = pci_system_init ();
  if (error != 0)
    {
      fprintf (stderr, "Couldn't initialize PCI system\n");
      exit (1);
    }

  pci_dev = get_pci_device ();

  /* Set up MCHBAR */
  if (IS_GEN6 (pci_dev->device_id) || IS_GEN7 (pci_dev->device_id))
    {
      mchbar_reg = MCHBAR_I965;
      pci_device_cfg_read_u32 (pci_dev, &temp, mchbar_reg);
      enabled = (temp & 1);

      if (enabled != 1)
        {
          /* let's enable it */
          pci_device_cfg_read_u32 (pci_dev, &temp, mchbar_reg);
          pci_device_cfg_write_u32 (pci_dev, temp | 1, mchbar_reg);
        }
      /* Do something...mmap, etc */

      /* clean-up MCHBAR */
      pci_device_cfg_read_u32 (pci_dev, &temp, mchbar_reg);
      temp &= ~1;
      pci_device_cfg_write_u32 (pci_dev, temp, mchbar_reg);

      /* Cleanup mmap, etc */
    }

  pci_system_cleanup ();

  return 0;
}
